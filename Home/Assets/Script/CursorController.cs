﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorController : MonoBehaviour {

	public Texture2D mouseNormal;
	public Texture2D mouseFoundObj;
	public Texture2D mouseFoundEnemy;
	public CursorMode cursorMode = CursorMode.Auto;
	public Vector2 hotSpot = Vector2.zero;
	public RawImage imagine;
	Vector2 imgDimension = new Vector2(50,50);

	//private bool isHeld=false;


	// Use this for initialization
	void Start () {
		setMouseNormal ();
	}
	
	// Update is called once per frame
	void Update () {
		if (GetMouseHoverObject (15) != null) {
			if (GetMouseHoverObject (15).tag.Equals ("Selectable")) {
				setMouseFoundObj ();
				if (Input.GetMouseButton (0)) {
					Destroy (GetMouseHoverObject (15));
				}
			} else if (GetMouseHoverObject (15).tag.Equals ("Enemy")) {
				setMouseFoundEnemy ();
			} else {
				setMouseNormal ();
			}
		} else
			setMouseNormal ();

	}

	GameObject GetMouseHoverObject(float range)
	{
		Vector3 position = gameObject.transform.position;
		RaycastHit raycastHit;
		Vector3 target = position + Camera.main.transform.forward * range;
		//target = Input.mousePosition +  Input.mousePosition. * range;
		if (Physics.Linecast (position, target, out raycastHit))
			return raycastHit.collider.gameObject;
		return null;
	}


	public void setMouseNormal(){
		imagine.texture = mouseNormal;
		imgDimension.x = 50;
		imgDimension.y = 50;
		imagine.rectTransform.sizeDelta = imgDimension;
	}

	public void setMouseFoundObj(){
		imgDimension.x = 30;
		imgDimension.y = 30;
		imagine.rectTransform.sizeDelta = imgDimension;
		imagine.texture = mouseFoundObj;

	}

	public void setMouseFoundEnemy(){
		imagine.texture = mouseFoundEnemy;
		imgDimension.x = 50;
		imgDimension.y = 50;
		imagine.rectTransform.sizeDelta = imgDimension;
	}

}
