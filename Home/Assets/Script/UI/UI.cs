﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour {

    public void StartB(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void QuitB() {
        Application.Quit();
    }
}
