﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIPoveste : MonoBehaviour {

    Text story;
    public Button play;
    public float fadeSpeed = 3f;

    Color txtColor;
    float percent;
    private void Awake()
    {
        play.gameObject.SetActive(false);
        story = GetComponent<Text>();
        txtColor = story.color;
        txtColor.a = 0;
        story.color = txtColor;
        percent = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (play.gameObject.activeInHierarchy == false)
        {
            if (percent < 1)
            {
                txtColor.a = percent;
                story.color = txtColor;
            }
            else
            {
                txtColor.a = 1;
                story.color = txtColor;
                play.gameObject.SetActive(true);
            }
            percent += Time.deltaTime * fadeSpeed;
        }
	}

    public void PlayB(string sceneName) {
        SceneManager.LoadScene(sceneName);
    }
}
