﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour {

	public float mouseSensitivity = 5;
	public Transform target;
	public float mediumDistFromTarget = 2;
	public Vector2 yAxisMinMax = new Vector2 (-40, 85);
	public float rotationSmoothTime = 0.12f;

	Vector3 rotationSmoothVelocity;
	Vector3 currentRotation;
	float distFromTarget;
	float distanceUsed;

	float xAxis ;
	float yAxis ;
	void Start(){
		distFromTarget = mediumDistFromTarget;
		SetCursorLock ();
	}

	void Update(){
		float distance = DistanceToCamera.distance3;

		if (distance <= distFromTarget) {
			distanceUsed = distance;
		} else
			distanceUsed = distFromTarget;

		//Debug.Log (distance);
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if (target != null)
        {
            xAxis += Input.GetAxis("Mouse X") * mouseSensitivity;
            yAxis -= Input.GetAxis("Mouse Y") * mouseSensitivity;
            yAxis = Mathf.Clamp(yAxis, yAxisMinMax.x, yAxisMinMax.y);

            currentRotation = Vector3.SmoothDamp(currentRotation, new Vector3(yAxis, xAxis), ref rotationSmoothVelocity, rotationSmoothTime);


            Vector3 targetRotation = new Vector3(yAxis, xAxis);
            transform.eulerAngles = currentRotation;

            transform.position = target.position - transform.forward * distanceUsed;
            //zummWithScroll();
            moveCameraWithScroll();
        }
	}

	void SetCursorLock()
	{
		
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible=true;
		Screen.fullScreen = true;
		//Screen.showCursor = !isLocked;
	}

	void zummWithScroll()
	{
		if (Input.GetAxis ("Mouse ScrollWheel") > 0 && GetComponent<Camera> ().fieldOfView>20) 
			GetComponent<Camera> ().fieldOfView--;
		else if(Input.GetAxis("Mouse ScrollWheel")<0 && GetComponent<Camera> ().fieldOfView<100)
			GetComponent<Camera> ().fieldOfView++;

	}
	void moveCameraWithScroll()
	{
		if (Input.GetAxis ("Mouse ScrollWheel") > 0 && distFromTarget<(mediumDistFromTarget+8))
			distFromTarget -= 0.5f;
		else if(Input.GetAxis("Mouse ScrollWheel")<0 && distFromTarget>(mediumDistFromTarget-8))
			distFromTarget += 0.5f;
	}

}
