﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	private const float YAngelMin = 0.0f;
	private const float YAngelMax = 50.0f;

	public Transform lookAt;
	public Transform camTransform;
	public float distandce = 6.0f;

	private Camera cam;
	private float currentX = 0.0f;
	private float currentY = 0.0f;
	//private float sensivityX = 4.0f;
	//private float sensivityY = 1.0f;

	private void Start()
	{
		camTransform = transform;
		cam = Camera.main;
	}

	private void Update()
	{
		currentX += Input.GetAxis("Mouse X");
		currentY += Input.GetAxis("Mouse Y");

		// nu putem sa dam mai sus de YAngelMax sau mai jos de YAngelMin!
		currentY = Mathf.Clamp (currentY, YAngelMin, YAngelMax);
		currentX = Mathf.Clamp (currentX, -YAngelMax, YAngelMax);
	}

	private void LateUpdate()
	{
		Vector3 dir = new Vector3(0, 8, -distandce);
		Quaternion rotation = Quaternion.Euler(currentY, currentX, 10);
		camTransform.position = lookAt.position + rotation * dir;
		camTransform.LookAt(lookAt.position);
	}

	
}
