﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivingEntity : MonoBehaviour, IDamageable {

    public int startingHealth;
    public ParticleSystem deathParticles;
    [HideInInspector]
    public int health;
    protected bool dead;

    public event System.Action OnDeath;

	// Use this for initialization
	protected virtual void Start () {
        health = startingHealth;
	}

    public void TakeHit(int damage) {
        health -= damage;
        if (health <= 0 && !dead) {
            health = 0;
            Die();
        }
    }

    protected void Die() {
        dead = true;
        if (gameObject.tag == "Player") {
            GameManager.instance.restarting = true;
            GameManager.instance.levelEnded = true;
        }
        Instantiate(deathParticles.gameObject, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

	// Update is called once per frame
	void Update () {
		
	}
}
