﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    public Text lives;
    public Text crystalsLeft;
    public Image fade;

    public PlayerController player;

    public static int crystalsTotal;
    public static int totalLife;

    bool levelStarting;
    public bool levelEnded;
    public bool restarting;
    float percent = 1;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start () {
        percent = 1;
        levelStarting = true;
        restarting = false;
        levelEnded = false;
	}
	
	// Update is called once per frame
	void Update () {
        
        

        /*if (player!=null && player.health == 0) {
            levelEnded = true;
        }*/
        if (levelStarting && !(levelEnded) && !SceneManager.GetActiveScene().name.Equals("Final")) {
            //Debug.Log("Am fost aici");
            crystalsLeft = GameObject.Find("Cristals").GetComponent<Text>();
            lives = GameObject.Find("Life").GetComponent<Text>();
            fade = GameObject.Find("Fade").GetComponent<Image>();
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
            totalLife = player.startingHealth;
            StartCoroutine(SceneStarted());
            
        }
        if (crystalsLeft != null)
        {
            crystalsLeft.text = "Crystals left: " + crystalsTotal;
        }
        if (lives != null) {
            lives.text = "Life: "+player.health+"/"+totalLife;
        }
        if (levelEnded) {
            StartCoroutine(SceneEnded());
        }
        if (crystalsTotal == 0)
        {
            levelEnded = true;
        }
    }

    IEnumerator SceneEnded() {
        //crystalsTotal = GameObject.FindGameObjectsWithTag("Selectable").Length;
        //float percent = 0;
        if (fade != null)
        {
            Color tmpCol = fade.color;
            tmpCol.a = percent;
            while (percent < 1)
            {
                percent += Time.deltaTime / 3f;
                tmpCol.a = percent;
                fade.color = tmpCol;
                yield return null;
            }
            percent = 1;
            tmpCol.a = 1;
            fade.color = tmpCol;
            levelStarting = true;
            if (restarting)
            {
                levelEnded = false;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            else
            {
                levelEnded = false;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
        }
        yield return null;
    }

    IEnumerator SceneStarted()
    {
        crystalsTotal = GameObject.FindGameObjectsWithTag("Selectable").Length;
        //Debug.Log(crystalsTotal);
        
        Color tmpCol = fade.color;
        tmpCol.a = percent;
        while (percent > 0)
        {
            percent -= Time.deltaTime/3f;
            tmpCol.a = percent;
            fade.color = tmpCol;
            yield return null;
        }
        percent = 0;
        tmpCol.a = 0;
        fade.color = tmpCol;
        levelStarting = false;
        yield return null;
    }

    
}
