﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPlayer : MonoBehaviour {
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy") {
            EnemyAI tmpE = other.transform.GetComponent<EnemyAI>();
            Rigidbody rb = tmpE.GetComponent<Rigidbody>();
            rb.AddForce(rb.transform.forward * (-50));
            tmpE.TakeHit(2);
        }
    }
}
