﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraPlayerMove : MonoBehaviour {

	public Transform target;
	public float lookSmooth = 0.09f;
	public Vector3 offsetFromTarget = new Vector3(0, 4, -8);
	public float xTilt = 10;
	public float rorationSpeed=2;

	//private float currentY = 0.0f;
	//private float previousY = 0.0f;
	Vector3 destination = Vector3.zero;
	PlayerController charController;
	float rotateVel = 0;

	void Start()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = true;
		SetCameraTarget(target);
	}

	void Update()
	{
		/*
		currentY += Input.GetAxis("Mouse X");
		if(previousY<currentY)
			transform.Rotate(0, rorationSpeed, 0);
		else if(previousY>currentY)
			transform.Rotate(0, -rorationSpeed, 0);
		previousY = currentY;
		*/
	}

	void SetCameraTarget(Transform t)
	{
		target = t;
		if (target != null) {
			if (target.GetComponent<PlayerController>()) {
				charController = target.GetComponent<PlayerController>();
			} else
				Debug.LogError ("That camera's target needs a character controller.");
		} else
			Debug.LogError ("You camera need a target.");
	}

	void LateUpdate()
	{
		//moving
		MovingToTarget();
		//rotating
		LookAtTarget();

	}

	void MovingToTarget()
	{
		destination = charController.transform.rotation * offsetFromTarget;
		destination += target.position;
		transform.position = destination;
	}

	void LookAtTarget()
	{
		float eulearYAngle = Mathf.SmoothDampAngle (transform.eulerAngles.y, target.eulerAngles.y, ref  rotateVel, lookSmooth);
		transform.rotation = Quaternion.Euler (transform.eulerAngles.x, eulearYAngle, 0);
	}





}
