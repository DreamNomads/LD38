﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyAI : LivingEntity {

    public enum State {
        Idle,
        Chasing,
        Attacking
    }

    //[HideInInspector]
    public State currentState;

    NavMeshAgent pathfinder;
    public Transform target;

    float attackDistanceThreshold = 1.5f;
    float timeBetweenAttack = 1;

    float nextAttackTime;
    float myCollisionRadius;
    float targetCollisionRadius;

	// Use this for initialization
	protected override void Start () {
        base.Start();
        pathfinder = GetComponent<NavMeshAgent>();

        currentState = State.Idle;
        target = GameObject.FindGameObjectWithTag("Player").transform;

        myCollisionRadius = GetComponentInChildren<BoxCollider>().size.z/2f;
        targetCollisionRadius = target.GetComponentInChildren<CapsuleCollider>().radius;

        StartCoroutine(UpdatePath());
    }
	
	// Update is called once per frame
	void Update () {
        if (target != null)
        {
            if (Time.time > nextAttackTime)
            {
                float sqrDstToTarget = (target.position - transform.position).sqrMagnitude;
                //Debug.Log("Putem ataca1 "+sqrDstToTarget);
                //Debug.Log("Putem ataca2 " + Mathf.Pow(attackDistanceThreshold + myCollisionRadius + targetCollisionRadius, 2));
                if (sqrDstToTarget < Mathf.Pow(attackDistanceThreshold + myCollisionRadius + targetCollisionRadius, 2))
                {
                    Debug.Log("Atacam");

                    nextAttackTime = Time.time + timeBetweenAttack;
                    StartCoroutine(Attack());
                }
            }
        }

        /*if (target != null) {
            transform.GetChild(0).transform.LookAt(target);
        }*/
	}

    IEnumerator Attack() {
        currentState = State.Attacking;
        //pathfinder.enabled = false;

        Vector3 originalPosition = transform.position;
        Vector3 dirToTarget = (target.position - transform.position).normalized;
        Vector3 attackPosition = target.position - dirToTarget * (myCollisionRadius);

        float attackSpeed = 3;
        float percent = 0;

        while (percent <= 1) {
            percent += Time.deltaTime * attackSpeed;
            float interpolation = (-Mathf.Pow(percent, 2) + percent) * 4;
            transform.position = Vector3.Lerp(originalPosition, attackPosition, interpolation);

            yield return null;
        }

        currentState = State.Chasing;
        pathfinder.enabled = true;
    }

    IEnumerator UpdatePath() {
        float refreshRate = .25f;
        while (target != null) {
            if (currentState == State.Chasing)
            {
                Vector3 dirToTarget = (target.position - transform.position).normalized;
                Vector3 targetPosition = target.position - dirToTarget * (myCollisionRadius + targetCollisionRadius);
                if (!dead)
                {
                    pathfinder.SetDestination(targetPosition);
                }
            }
            yield return new WaitForSeconds(refreshRate);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player") {
            
            PlayerController pl = other.GetComponent<PlayerController>();
            if(!pl.isAttacking)
            pl.TakeHit(1);
        }
    }
}
