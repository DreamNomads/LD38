﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerController : LivingEntity
{
	static Animator anim ;

	public float moveSpeed = 10;
	public float runSpeed = 20;
	public float jumpHight = 8;
	public float rorationSpeed = 2;


	private float move = 0;
	private Rigidbody rigBody;
	private List<string> listAnimation=new List<string>();
	private CapsuleCollider controller;

	//private float currentX = 0.0f;
	//private float previousX = 0.0f;

	private Transform cameraT;
	private float turnSmoothTime = 0.1f;
	private float turnSmoothVelocity;

    public BoxCollider Attack;
    public bool isAttacking;

	protected override void Start()
	{
        base.Start();
		move = moveSpeed;
		controller = GetComponent<CapsuleCollider> ();
		anim = GetComponent<Animator> ();
		rigBody = GetComponent<Rigidbody>();
		for (int i = 0; i < anim.parameterCount; i++)
			listAnimation.Add (anim.GetParameter (i).name);
		cameraT = Camera.main.transform;
        Attack.gameObject.SetActive(false);
	}

	// Update is called once per frame
	void Update()
	{

		if (Input.GetKey (KeyCode.LeftShift))
			move = runSpeed;
		else
			move = moveSpeed;

        if (anim.GetBool("isAttacking") == true)
        {
            Attack.gameObject.SetActive(true);
            isAttacking = true;
        }
        else {
            Attack.gameObject.SetActive(false);
            isAttacking = false;
        }
		if (Input.GetKeyDown (KeyCode.Space) || Input.GetKey (KeyCode.A)
		   || Input.GetKey (KeyCode.D)//|| Input.GetKey(KeyCode.S)
		   || Input.GetKey (KeyCode.W) || Input.GetMouseButton (0)) {
			
			
			if (Input.GetMouseButton (0)) {
				setAnimation ("isAttacking");
			}
			if (Input.GetKey (KeyCode.A))
				transform.Rotate (0, -rorationSpeed, 0);
			else if (Input.GetKey (KeyCode.D))
				transform.Rotate (0, rorationSpeed, 0);
			//else transform.rotation = Quaternion.Euler(0,0,0);
			if (Input.GetKey (KeyCode.W)) {
				transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y,cameraT.eulerAngles.y, ref turnSmoothVelocity,turnSmoothTime);
				setAnimation ("isWalking");
				transform.Translate (0, 0, move * Time.deltaTime);

			}
			if (Input.GetKey (KeyCode.S)) {

				setAnimation ("isWalking");
				transform.Translate (0, 0, -move * Time.deltaTime);

			}
			if (Input.GetKeyDown (KeyCode.Space) && IsGrounded ()) {
				setAnimation ("isJumping");
				Vector3 a;
				a.x = rigBody.velocity.x;
				a.y = rigBody.velocity.y + jumpHight;
				a.z = rigBody.velocity.z;
				rigBody.velocity = a;
			}
		} else 
		{
			setAnimation ("isIdle");

		}

		/*
		currentX += Input.GetAxis("Mouse X");
		if(previousX<currentX)
			transform.Rotate(0, rorationSpeed, 0);
		else if(previousX>currentX)
			transform.Rotate(0, -rorationSpeed, 0);
		previousX = currentX;
		*/
		/*
		Vector3 dir = new Vector3(0, 0, -distandce);
		Quaternion rotation = Quaternion.Euler(0, currentX, 10);
		Debug.Log (rotation.ToString());
			transform.Rotate (0, (rotation * dir).y, 0);
		*/



		

	}
	void setAnimation(string name){
		
		for (int i = 0; i < listAnimation.Count; i++)
			if (!listAnimation [i].Equals (name))
				anim.SetBool (listAnimation [i], false);
		anim.SetBool (name, true);
	}

	bool IsGrounded()
	{
		Vector3 leftRayStart;
		Vector3 rightRayStart;
		leftRayStart = controller.bounds.center;
		rightRayStart = controller.bounds.center;
		leftRayStart.x += controller.bounds.extents.x;
		rightRayStart.x -= controller.bounds.extents.x;
		//Debug.DrawLine (leftRayStart,Vector3.down, Color.black);
		//Debug.DrawLine (rightRayStart,Vector3.down, Color.yellow);
		if (Physics.Raycast (leftRayStart, Vector3.down, (controller.height / 2) + 0.02f))
			return true;
		if (Physics.Raycast (rightRayStart,Vector3.down, (controller.height / 2) + 0.02f))
			return true;
		return false;
	}

	
	void OnTriggerEnter(Collider col)
	{
        if (col.tag == "Selectable") {
            GameManager.crystalsTotal--;
            Destroy(col.gameObject);
        }
	}
	
}
