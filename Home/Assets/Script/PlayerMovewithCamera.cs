﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovewithCamera : MonoBehaviour {
	public float moveSpeed = 2;
	public float runSpeed = 6;

	public float turnSmoothTime = 0.2f;
	float turnSmoothVelocity;

	private List<string> listAnimation=new List<string>();
	private CapsuleCollider controller;

	public float speedSmoothTime = 0.1f;
	float speedSmoothVelocity;
	float currentSpeed = 10;

	Animator anim;
	Transform cameraT;

	// Use this for initialization
	void Start () {
		controller = GetComponent<CapsuleCollider> ();
		anim = GetComponent<Animator> ();
		cameraT = Camera.main.transform;
		for (int i = 0; i < anim.parameterCount; i++)
			listAnimation.Add (anim.GetParameter (i).name);
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey(KeyCode.W)) {
			transform.Translate (transform.forward * currentSpeed * Time.deltaTime, Space.World);
		}
		transform.Rotate (Vector3.up, 0.0f);
		transform.rotation = Quaternion.Euler(0,0,0);

		
	}


	void setAnimation(string name){

		for (int i = 0; i < listAnimation.Count; i++)
			if (!listAnimation [i].Equals (name))
				anim.SetBool (listAnimation [i], false);
		anim.SetBool (name, true);
	}

	bool IsGrounded()
	{
		Vector3 leftRayStart;
		Vector3 rightRayStart;
		leftRayStart = controller.bounds.center;
		rightRayStart = controller.bounds.center;
		leftRayStart.x += controller.bounds.extents.x;
		rightRayStart.x -= controller.bounds.extents.x;
		//Debug.DrawLine (leftRayStart,Vector3.down, Color.black);
		//Debug.DrawLine (rightRayStart,Vector3.down, Color.yellow);
		if (Physics.Raycast (leftRayStart, Vector3.down, (controller.height / 2) + 0.02f))
			return true;
		if (Physics.Raycast (rightRayStart,Vector3.down, (controller.height / 2) + 0.02f))
			return true;
		return false;
	}
}
