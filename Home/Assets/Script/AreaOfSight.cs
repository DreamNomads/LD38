﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaOfSight : MonoBehaviour {

    public EnemyAI theEnemy;

    private void Start()
    {
        theEnemy = transform.parent.GetComponent<EnemyAI>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player") {
            Debug.Log("Entered");
            theEnemy.currentState = EnemyAI.State.Chasing;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        OnTriggerEnter(other);
    }
}
